import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.Matchers.is;


public class Apitest {
    String host = "http://localhost:8080";
    String[] cpf = {
            "97093236014",
            "60094146012",
            "84809766080",
            "62648716050",
            "26276298085",
            "01317496094",
            "55856777050",
            "19626829001",
            "24094592008",
            "58063164083"};

    @Test
    public void consultarComRestriçãoPeloCPF() {
        for (String valor : cpf) {
            when().get(String.format("%s/api/v1/restricoes/%s", host, valor)).
                    then().log().all()
                    .assertThat().statusCode(200)
                    .and().body("mensagem", is(String.format("O CPF %s tem problema", valor)));
        }
    }

    @Test
    public void consultarSemRestriçãoPeloCPF() {
        when().get(String.format("%s/api/v1/restricoes/27093236014", host)).
                then().log().all()
                .assertThat().statusCode(204);
    }

    @Test
    public void criarUmaSimulação() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulano de Tal\",\n" +
                        "  \"cpf\": 27093236014,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 1200,\n" +
                        "  \"parcelas\": 3,\n" +
                        "  \"seguro\": true\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(201);
    }

    @Test
    public void criarSimulaçãoCpfDuplicado() {
        //este teste leva em consideração o cpf que ja vem no backend do projeto por default
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulanos de Tal\",\n" +
                        "  \"cpf\": 66414919004,\n" +
                        "  \"email\": \"emails@email.com\",\n" +
                        "  \"valor\": 1203,\n" +
                        "  \"parcelas\": 4,\n" +
                        "  \"seguro\": false\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(is(409))
                .body("mensagem", is("CPF já existente"));
        /**
         * Uma simulação para um mesmo CPF retorna um HTTP Status 409 com a mensagem
         * "CPF já existente"
         */
    }

    @Test
    public void criarSimulaçaoValorAlto() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulano de Tal\",\n" +
                        "  \"cpf\": 970932360141,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 41000,\n" +
                        "  \"parcelas\": 90,\n" +
                        "  \"seguro\": false\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(is(400))
                .body("erros.valor", is("Valor deve ser menor ou igual a R$ 40.000"));
    }

    @Test
    public void criarSimulaçaoValorBaixo() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulano de Tal\",\n" +
                        "  \"cpf\": 9209323601412,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 999,\n" +
                        "  \"parcelas\": 50,\n" +
                        "  \"seguro\": false\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(is(400));
        //.body("valor", is("Valor deve ser igual ou maior a R$ 1.000"));
    }

    @Test
    public void criarSimulaçaoparcelaMin() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulano de Tal\",\n" +
                        "  \"cpf\": 920932360143,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 1999,\n" +
                        "  \"parcelas\": 1,\n" +
                        "  \"seguro\": false\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(is(400))
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"));
    }

    @Test
    public void criarSimulaçaoparcelaMax() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"Fulano de Tal\",\n" +
                        "  \"cpf\": 9209323601454,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 999,\n" +
                        "  \"parcelas\": 90,\n" +
                        "  \"seguro\": false\n" +
                        "}")
                .when()
                .post(String.format("%s/api/v1/simulacoes", host))
                .then().log().all().statusCode(is(400));
        //.body("erros.parcelas", is("Parcelas deve ser igual ou menor que 48"));

    }

    @Test
    public void alterarSimulaçao() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"fulano testado\",\n" +
                        "  \"seguro\": true\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66414919004", host))
                .then().log().all().statusCode(is(200))
                .body("nome", is("fulano testado"));
    }

    @Test
    public void alterarSimulaçaoInexistente() {
        given().contentType("application/json")
                .body("{\n" +
                        "  \"nome\": \"fulano testado\",\n" +
                        "  \"seguro\": true\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66400009004", host))
                .then().log().all().statusCode(is(404))
                .body("mensagem", is("CPF não encontrado"));
        /**
         * Se o CPF não possuir uma simulação o HTTP Status 404 é retornado com a
         * mensagem "CPF não encontrado"
         */
    }

    @Test
    public void alterarSimulaçaoValorAlto() {
        given().contentType("application/json")
                .body("{\n" +
                        "    \"nome\": \"fulaninho testado2\",\n" +
                        "    \"email\": \"fulanos@gmail.com\",\n" +
                        "    \"valor\": \"41000\",\n" +
                        "    \"parcelas\": 4,\n" +
                        "    \"seguro\": false\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66414919004", host))
                .then().log().all().statusCode(is(200))
                .body("valor", is("41000.0"));
    }

    @Test
    public void alterarSimulaçaoValorBaixo() {
        given().contentType("application/json")
                .body("{\n" +
                        "    \"nome\": \"fulaninho testado2\",\n" +
                        "    \"email\": \"fulanos@gmail.com\",\n" +
                        "    \"valor\": \"999\",\n" +
                        "    \"parcelas\": 4,\n" +
                        "    \"seguro\": false\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66414919004", host))
                .then().log().all().statusCode(is(200))
                .body("valor", is("999.0"));
    }

    @Test
    public void alterarSimulaçaoParcelaMax() {
        given().contentType("application/json")
                .body("{\n" +
                        "    \"nome\": \"fulaninho testado2\",\n" +
                        "    \"email\": \"fulanos@gmail.com\",\n" +
                        "    \"valor\": 5000,\n" +
                        "    \"parcelas\": 99,\n" +
                        "    \"seguro\": false\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66414919004", host))
                .then().log().all().statusCode(is(400));
        //.body("erros.parcelas", is("Parcelas deve ser igual ou menor que 48"));
    }

    @Test
    public void alterarSimulaçaoParcelaMin() {
        given().contentType("application/json")
                .body("{\n" +
                        "    \"nome\": \"fulaninho testado2\",\n" +
                        "    \"email\": \"fulanos@gmail.com\",\n" +
                        "    \"valor\": 5000,\n" +
                        "    \"parcelas\": 1,\n" +
                        "    \"seguro\": false\n" +
                        "}")
                .when()
                .put(String.format("%s/api/v1/simulacoes/66414919004", host))
                .then().log().all().statusCode(is(400))
                .body("erros.parcelas", is("Parcelas deve ser igual ou maior que 2"));
    }

    @Test
    public void consultarLista() {
        Response response = when().get(String.format("%s/api/v1/simulacoes", host));
        response.then().log().all();
        int size = response.jsonPath().get("size()");
        if (response.statusCode() == 204) {
            Assert.assertTrue("Lista não apresenta uma ou mais simulacoes cadastradas", size == 0);
        }
        Assert.assertTrue("Lista não apresenta uma ou mais simulacoes cadastradas", size >= 1);
    }
    @Test
    public void consultarSimulacaoCPF() {
        //Response response = when().get(String.format("%s/api/v1/simulacoes/27093236014", host));
        when().get(String.format("%s/api/v1/simulacoes/27093236014", host)).
                then().log().all()
                .assertThat().statusCode(200)
                .and().body("cpf", is("27093236014"));
    }
    @Test
    public void consultarSimulacaoCPFFail() {
        //Response response = when().get(String.format("%s/api/v1/simulacoes/27093236014", host));
        when().get(String.format("%s/api/v1/simulacoes/2709323612014", host)).
                then().log().all()
                .assertThat().statusCode(404)
                .and().body("mensagem", is("CPF 2709323612014 não encontrado"));
    }
    @Test
    public void removerSimulaçao() {
        when().delete(String.format("%s/api/v1/simulacoes/11", host)).
                then().log().all()
                .assertThat().statusCode(204);
        /**
         * Retorna o HTTP Status 204 se simulação for removida com sucesso
         */
    }
    @Test
    public void removerSimulaçaoFail() {
        when().delete(String.format("%s/api/v1/simulacoes/500", host)).
                then().log().all()
                .assertThat().statusCode(404)
                .and().body("mensagem", is("Simulação não encontrada"));
        /**
         * Retorna o HTTP Status 404 com a mensagem "Simulação não encontrada" se não
         * existir a simulação pelo ID informado
         */
    }

}

